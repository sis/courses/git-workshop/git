---
author: Gerhard Bräunlich
lang: en-US
title: git & gitlab
subtitle: https://siscourses.ethz.ch/git-workshop
...

# git

<small>Distributed file versioning</small>

* Local versioning system
* Sync with remote repository
* Support for collaborative work

# Why?

```txt
paper
│
│
│
└── paper.tex






```

# Why?

```txt
paper
│
│
│
├── paper.tex
├── paper-v02.tex
├── ...
└── paper-v(n).tex



```

# Why?

```txt
paper
├── paper-2018-03-02.tex
├── paper-2018-03-15.tex
├── paper-2018-03-16.tex
├── paper.tex
├── paper-v02.tex
├── ...
└── paper-v(n).tex



```

# Why?

```txt
paper
├── paper-2018-03-02.tex
├── paper-2018-03-15.tex
├── paper-2018-03-16.tex
├── paper.tex
├── paper-v02.tex
├── ...
├── paper-v(n).tex
└── paper-v(n+1).tex
```

# Why?

🤯 Filesystem Chaos

```txt
paper
├── ...
├── paper-2018-03-(n).aux
├── paper-2018-03-(n).log
├── paper-2018-03-(n).pdf
├── paper-2018-03-(n).tex
└── ...
```

# Why?

🤔 Branching: Missing history

![](./img/paper-branch.svg){.element: width="50%"}


# Why?

🤢 Redundancy of information

```tex  {data-caption="paper-2018-03-(n).tex"}
...
content
...
```

```tex  {data-caption="paper-2018-03-(n+1).tex"}
...
content
added content
...
```

# Setup

|    |                                                               |
|----|---------------------------------------------------------------|
| 🐧 | `apt-get install git`{.console} / `yum install git`{.console} |
| 🪟 | [gitforwindows.org](https://gitforwindows.org/)               |
| 🍏 | [git-scm.com/download/mac](https://git-scm.com/download/mac)  |


# First time setup

```sh {data-caption="set your name and email"}
git config --global user.name "Chuck Norris"
git config --global user.email "chucknorris@roundhouse.gov"
```

Also can be done per repository (omit `--global`)

```sh {data-caption="set your preferred editor"}
git config --global core.editor "code --wait"
```

---

Initialize a new repository:

```sh
git init
```

Clone an already published project (repository)

```sh
git clone git@gitlab.ethz.ch:sis/courses/git-workshop/git.git
```

```sh
git clone https://gitlab.ethz.ch/sis/courses/git-workshop/git.git
```

# Commits

Idea: Organize changes to your project in "commits".

**commit**: "consistent" unit of change.

::: incremental

* As small as possible
* As large as necessary
* Should not break code / test / builds
* Sometimes this cannot be avoided

:::

---

## Commit messages

::: incremental

* **rule of 👍**: commit message is short and does not contain any
"and"-connected statements
* Conventions (sources:
  [1](https://cbea.ms/git-commit/#imperative),
  [2](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html),
  [3](https://www.git-scm.com/book/en/v2/Distributed-Git-Contributing-to-a-Project#_commit_guidelines),
  [4](https://github.com/torvalds/subsurface-for-dirk/blob/master/README.md#contributing),
  [5](http://who-t.blogspot.co.at/2009/12/on-commit-messages.html),
  [6](https://github.com/erlang/otp/wiki/writing-good-commit-messages),
  [7](https://github.com/spring-projects/spring-framework/blob/30bce7/CONTRIBUTING.md#format-commit-messages)):
    + Separate subject from body with a blank line
    + Limit the subject line to 50 characters
    + Capitalize the subject line
    + Do not end the subject line with a period
    + Use the [imperative mood](https://git.kernel.org/pub/scm/git/git.git/tree/Documentation/SubmittingPatches?id=HEAD#n307) in the subject line
    + Wrap the body at 72 characters
    + Use the body to explain what and why vs. how

:::

---

:::{.r-hstack .extend .align-top}

::: history
👎 Bad examples:

::: x-large

* Changes of 2021-12-01
* Changes of 2021-12-02
* Changes of 2021-12-02 (part 2)

:::

  ---

::: x-large

* New function / Bugfix
* Other function / Remove old code
* Fix new function / Clean comments

:::

:::

::: history

👍 Good examples:

::: x-large

* Fix bug in routine A
* Add function print_42()
* Rename argument of print_42()
* Reformat codebase

:::

:::

:::

---

👎 Bad example:

<figure>
[![](https://imgs.xkcd.com/comics/git_commit_2x.png){title="Merge branch 'asdfasjkfdlas/alkdjf' into sdkjfls-final"}](https://xkcd.com/1296/){target="blank_"}

<figcaption>[xkcd.com/1296: Git Commit](https://xkcd.com/1296/){target="blank_"}</figcaption>
</figure>

---

:::{.r-hstack .extend .align-top}

::: history
👎 Bad examples:

::: x-large

* [This patch] fixes a segfault in routine A

:::

  ---

::: x-large

* [I] fixed a segfault in routine A

:::

:::

::: history

👍 Good example:

::: x-large

* Fix a segfault in routine A

:::

:::

:::

# Local Workflow

::: incremental

1. Stage files (mark files to be included in the next commit):

    ```sh
    git add file1 file2 ... # if not yet tracked, tracking starts here
    ```

2. Commit changes:

    ```sh
    git commit -m "Description of the changes"
    ```

3. Continue work
4. Back to step 1

:::

---

To view what is staged / changed, use

```sh
git status
```

::: fragment

<pre>
<code class="sourceCode">On branch main
Your branch is up to date with 'origin/main'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	<span class="st">modified:   paper.tex</span>

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	<span class="fu">modified:   img/figure.svg</span>

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	<span class="co">img/new-figure.svg</span></code>
</pre>

:::

---

![](img/track-and-stage.svg)

---

To stage all tracked, changed files, use

```sh
git add -u # safer than git add .
```

::: fragment
Tipp: you also can stage only parts of changed files:

```sh
git add -p file1 file2 ...
# -e for to even edit the diff
```
:::
::: fragment
To untrack but not remove the file, use

```sh
git rm --cached <file>
```

:::

# Viewing changes

::: fragment
```{.sh caption="List changed files / staged files (marked for a commit)"}
git status
```
:::

::: fragment
```{.sh caption="View history"}
git log
```
:::

---

```{.sh caption="View changes contained in one commit"}
git log <commit-ref>
# e.g. <commit-ref>=11089693 (commit-ref is longer but
# max. first 8 chars suffice per repo)
```

::: fragment
```{.sh caption="View history of commit messages to a single file"}
git log filename
```
:::

::: fragment
```{.sh caption="View history of changes to a single file"}
git log -p filename
```
:::

---

Pro-tip: Customize `git log`:

```sh
git config --global alias.lg "log --color \
  --graph \
  --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s ' \
                  '%Cgreen(%cr) %C(bold blue)<%an>%Creset' \
  --abbrev-commit"
```

::: fragment

(omit command line options to see, if you actually like the effect)

:::

This enables:

```sh
git lg
```

---

```sh
git diff               # Changes relative to the last commit
git diff <commit-ref>  # Changes relative to <commit-ref>
git diff <branch-name> # Changes relative to <branch-name>
```

::: fragment
```{.sh caption="View staged changes relative to last commit"}
git diff --staged # or --cached
```
:::

::: fragment
```{.sh caption="View when and who changed file contents line-by-line"}
git blame filename
# Add -L 40,60 to only see changes between line 40 and 60
# Add -L '/^double f(double x)/' to only see changes on lines
#  matching the regexp
# Add --since=3.weeks to only show changes 3 weeks back
```
:::

# Exercises

* Visit [learngitbranching.js.org](https://learngitbranching.js.org)
  and do "1. Introduction to Git commits"
* Execute

  ```sh
  git clone https://github.com/eficode-academy/git-katas.git
  cd git-katas
  ```
  
  and do exercises [Basic
  Commits](https://github.com/eficode-academy/git-katas/blob/master/basic-commits/README.md)
  and [Basic Staging](https://github.com/eficode-academy/git-katas/blob/master/basic-staging/README.md).

---

## Things that should NOT be versioned

* Intermediate files / by-products
* Final binaries
* Platform specific files / directories
* User specific directories
* ⚠️ **Plain text passwords**

<aside class="notes">Greetings from ae108-legacy</aside>

---

Use `.gitignore` to declare this sort of files:

```
# Intermediate files / by-products
*.o
*.obj
*.log
# Final binaries
*.pdf
*.exe
# Platform specific files / directories
Thumbs.db
.DS_Store
__MACOSX/
# User specific directories
.vscode/
```

Also version `.gitignore`!

---

Templates for `.gitignore` available at

[https://github.com/github/gitignore](https://github.com/github/gitignore)

---

## Large data files

E.g. large images for papers

Use [git-lfs: git-lfs.com](https://git-lfs.com/) (Large File Storage)

---

# Exercise: [git-katas/ignore](https://github.com/eficode-academy/git-katas/tree/master/ignore)

---

## When things should NOT go into `.gitignore`

```txt
project
├── README.md    # <- versioned
├── src
│   └── main.cpp # <- versioned
├── build        # <- binaries => should go to .gitignore
│   └── ...
└── private      # Uncommon / not used by most other devs
    ├── my_private_garbage # 🗑
    └── ...
```

::: fragment
Use the "private" / untracked version of `.gitignore`:

```
.git/info/exclude
```

:::


# Git for collaboration

**When**:

- work on individual features
- try out alternative solutions
- trace and fix bugs

---

... **Then**: git branches

![](./img/branch.svg){.element: width="20%"}

Create a new branch out of the current git state:

```sh
git switch -c branch-name
```

Once you continued work and want to go back to the state of a branch:

```sh
git switch branch-name
```

---

Go back to previous branch:

```sh
git switch -
```

Delete branch:
```sh
git branch -d branch-name
```

Rename branch:
```sh
git branch -m old-branch-name new-branch-name
```

---

# Exercise

* [learngitbranching.js.org](https://learngitbranching.js.org)
  and do "2. Branching in Git"
* [git-katas/basic-branching](https://github.com/eficode-academy/git-katas/blob/master/basic-branching/README.md)

---

# Publishing on gitlab.ethz.ch

* Log in to [https://gitlab.ethz.ch](https://gitlab.ethz.ch){target="_blank"}
* <span class="gitlab-button">New project</span>
* Follow the instructions there under "Push an existing Git repository"
  ```sh
  git remote add origin git://gitlab.ethz.ch/username/project
  git push -u origin --all
  ```
* To also publish different branches:
  ```sh
  git switch other-branch
  git push -...
  ```

---

## Exercise

:::large

* Create a new local git repository.
* Make an initial commit with commit message `"1st commit"`,
  containing one single file: `README.md`. Content:
  ```md
  # Top Secret Project
  ```
* Create a 2nd branch named "secret".
* Change the file `README.md`, so it now contains:
  ```md
  # Top Secret Project

  We don't yet know, what it is good for.
  ```
* Commit the changes (commit message: `"Add description to README.md"`)
* Publish both branches (main, secret) to a private project on
  gitlab.ethz.ch (user space).
  
  Dont forget to uncheck the box "create README.md"!
* Give me access to the repos, you created (`@brgerhar`).

:::

# Distributed synchronisation workflow

How to share changes with others?

---

<div class="r-stack">
<img src="img/distributed-workflow/01.svg" class="fragment fade-out" data-fragment-index="0">
<img src="img/distributed-workflow/02.svg" class="fragment current-visible" data-fragment-index="0">
<img src="img/distributed-workflow/03.svg" class="fragment current-visible">
<img src="img/distributed-workflow/04.svg" class="fragment current-visible">
<img src="img/distributed-workflow/05.svg" class="fragment current-visible">
<img src="img/distributed-workflow/06.svg" class="fragment current-visible">
<img src="img/distributed-workflow/07.svg" class="fragment current-visible">
<img src="img/distributed-workflow/08.svg" class="fragment current-visible">
<img src="img/distributed-workflow/09.svg" class="fragment current-visible">
<img src="img/distributed-workflow/10.svg" class="fragment current-visible">
<img src="img/distributed-workflow/11.svg" class="fragment current-visible"></div>

# Rebase vs merge: rebase

![](./img/rebase.svg)

```sh
git switch feature
git rebase main
```

# Rebase vs merge: merge

![](./img/merge.svg)

```sh
git switch main
git merge feature
```

# Exercise

[learngitbranching.js.org](https://learngitbranching.js.org):

* 3. Merging in Git
* 4. Rebase introduction
* Rebasing over 9000 times

# Resolving conflicts

![](./img/conflict.svg)

A commit in a feature branch changes the same line as a change
commited to the main branch in the meantime.

---

Example

:::{.r-hstack .extend}

``` {.diff .extend}
commit d97360f (HEAD -> main)
Author: Student B <b@ethz.ch>
Date:   Sat Dec 11 17:13 2021

    Add authors

diff --git a/AUTHORS.md b/AUTHORS.md
index 0ca9600..61e8a1b 100644
--- a/AUTHORS.md
+++ b/AUTHORS.md
@@ -1 +1,4 @@
 # Authors
+
+* Student B
+* Student A
```

``` {.diff .extend}
commit 6b3c423 (HEAD -> feature-a)
Author: Student A <a@ethz.ch>
Date:   Sat Dec 11 17:13 2021

    Add authors

diff --git a/AUTHORS.md b/AUTHORS.md
index 0ca9600..a15a7dd 100644
--- a/AUTHORS.md
+++ b/AUTHORS.md
@@ -1 +1,4 @@
 # Authors
+
+* Student A
+* Student B
```

:::

---

During merge / rebase, git detects conflicts and marks
affected lines in affected files:

```md
# Authors

<<<<<<< HEAD
* Student B
* Student A
=======
* Student A
* Student B
>>>>>>> 6b3c423 (Add authors)
```

::: fragment

After resolving the conflict, the file could look like this:

```md
# Authors

Professor X et al.
```

:::

---

# What is "origin/"?

![git also tracks a state of the state of the remote.](./img/remote.svg)

---

![](./img/git-commands.svg)

---

# Branching workflow

Idea: Only work on "feature" branches only, let a lead developer
merge the changes into a common branch.

![](./img/feature-branches.svg)

See also [this discussion](https://about.gitlab.com/topics/version-control/what-are-gitlab-flow-best-practices/){target="_blank"}.

# GitLab issue-oriented workflow

::: incremental

* <span class="gitlab-button">New issue</span> (Task: fix a bug, add a feature)
* <span class="gitlab-button">Create merge request</span> (from within the issue)
  -> new feature branch on the remote repository
* Fetch the automatically created branch

    ```sh
    git fetch   # or git pull
    # you will get a list of all
    # newly created remote branches
    git switch <branch-name>
    ```

:::

---

* Work on the feature branch locally (containing possibly multiple
  commits to a new feature).

::: incremental

* Push changes: `git push`
* <span class="gitlab-link">Mark as ready</span>Mark the Merge request as "ready" and ask the lead developer / colleague (reviewer) for a review
* Fix all issues found by the reviewer
* After approval: merge the merge request

:::

---

## Live demo

[https://gitlab.ethz.ch/sis/courses/git-workshop/demo-project](https://gitlab.ethz.ch/sis/courses/git-workshop/demo-project){target="_blank"}

---

## Conventions

* Don't change commits on shared branches (`main`)
* Clean up the history on "self-owned" branches before submitting for review
* Stick to git commit message conventions

---

### Clean History

::: fragment

```sh {data-caption="fix the last commit"}
git commit --amend --no-edit # Without `--no-edit`, you also
# can alter the commit message.
```
:::

::: fragment
```sh {data-caption="prepare fixes for older commits"}
git commit --fixup=<commit-ref>
```
:::

::: fragment
```sh {data-caption="squash fixup commits into the older commits"}
git rebase -i <ref> --autosquash
# <ref>: branch / commit ref before commit to be rewritten
```
:::

::: fragment
```sh {data-caption="delete / edit / change message of older commits"}
git rebase -i <ref>
# <ref>: branch / commit ref before commits to be rewritten
```

:::

---

Rewriting the history needs a "force" push:

```sh
git push --force
```
---

others (e.g. reviewer) can update using a "force pull":

```sh
git switch source-branch # if not already on the branch
git fetch
git reset --hard origin/source-branch
```

---

::: info

When you rewrite a commit, a force push is crucial.
A normal push will be rejected and you will be instructed to do a pull
first (see message below). This is not what we want to accomplish in this situation.
Ignore it and do the force push.

:::

```
 ! [rejected]        main -> main (non-fast-forward)
error: failed to push some refs to '...'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

# Exercise

Visit

[gitlab.ethz.ch/sis/courses/git-workshop/exercises/merge_requests](https://gitlab.ethz.ch/sis/courses/git-workshop/exercises/merge_requests){target="_blank"}

and follow the instructions there.


# Links

* [atlassian git cheat sheet](https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet)
* [Git Book](https://git-scm.com/book/en/v2)
* [Interactive task-oriented course with visual help](https://learngitbranching.js.org/)

# Acknowledgements

Slides:

* [Reveal.js](https://revealjs.com/), MIT
* [Pandoc](https://pandoc.org/), GPLv2+, BSD 3-clause

Mik Rybinski for a careful review

# The End

:::{.r-hstack .extend}

<figure>
[![](https://imgs.xkcd.com/comics/git.png){title="If
that doesn't fix it, git.txt contains the phone number of a friend of
mine who understands git. Just wait through a few minutes of 'It's
really pretty simple, just think of branches as...' and eventually
you'll learn the commands that will fix everything."}](https://xkcd.com/1597/){target="blank_"}

<figcaption>[xkcd.com/1597: Git](https://xkcd.com/1597/){target="blank_"}</figcaption>
</figure>

::: fragment

## 📊 Slides

✨ Live version: [siscourses.ethz.ch/git-workshop](https://siscourses.ethz.ch/git-workshop)

📖 Source: [gitlab.ethz.ch/sis/courses/git-workshop/git](https://gitlab.ethz.ch/sis/courses/git-workshop/git)

:::

:::
