all: index.html

%.html: %.md header.html Makefile
	pandoc \
            --standalone \
            -o $@ \
            -t revealjs \
            --slide-level=2 \
            --shift-heading-level-by=1 \
            --highlight-style=espresso \
            -H header.html \
            -V transition=none \
            -V theme=white \
            -V revealjs-url=https://unpkg.com/reveal.js@5.1.0 \
            -V slideNumber=true \
            $<

# For an offline version:
# yarn add reveal.js
# Then set -V revealjs-url=./node_modules/reveal.js

#             --embed-resources \
