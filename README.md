# Git & GitLab

As a first goal, you should be able to use git to
* version your code locally,
* publish your code (in our case gitlab).

Second goal: You are able to contribute to a common project (internal
working group project or open source project).
This includes following some common conventions / procedures to
prevent chaos (how should changes be grouped together, how should
those changes be annotated). We will see how the GitLab web application
guides you through a common workflow.

As a hands-on exercise, you will:
* Publish your own example code to the group space at gitlab.ethz.ch.
* Contribute to an existing example project.

# Pre-Requisites

* git

    |     |     |
    | --- | --- |
    | 🐧  | `apt-get install git` / `yum install git` |
    | 🪟  | [https://git-scm.com/download/win/](https://git-scm.com/download/win/) |
    | 🍏  | [git-scm.com/download/mac](https://git-scm.com/download/mac) |
